﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class DesplayWinOrLose : MonoBehaviour {

    [SerializeField]
    private Text winOrLose;


    public void DisplayResult(string result)
    {
        winOrLose.gameObject.SetActive(true);
        winOrLose.text = result;
    }
}
