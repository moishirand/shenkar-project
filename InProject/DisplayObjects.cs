﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DisplayObjects : MonoBehaviour {
    [HideInInspector]
    public GameObject capsule;
    [HideInInspector]
    public GameObject enemy;
    [HideInInspector]
    public bool isAlice;
     

    [SerializeField]
    private GameObject camera;
    [SerializeField]
    private Animator damegeAnim;


    public float speedEnemy;

    //private float dist;

	// Use this for initialization
	void Start () {
        speedEnemy = 5;

    }
	
	// Update is called once per frame
	void Update () {
		if(capsule != null)
        {
            CheckDistFromCapsule();
        }

        if(enemy != null)
        {
            CheckDistFromEnemy();
        }

        
	}

    private void CheckDistFromEnemy()
    {
        float dist = Vector3.Distance(enemy.transform.position, camera.transform.position);
        if (dist > 1 && dist < 3)
        {
            enemy.GetComponentInChildren<SkinnedMeshRenderer>().enabled = true;
            EnemyDisplay();
        }

        if(dist > 0 && dist < 1)
        {
            AttackEnemy();
            
        }

    }


    private void CheckDistFromCapsule()
    {
        float dist = Vector3.Distance(capsule.transform.position, camera.transform.position);
        //Debug.Log(dist);

        if (dist > 0 && dist < 1)
        {
            capsule.GetComponent<MeshRenderer>().enabled = true;
            CapsuleDisplay();
        }
    }

    private void EnemyDisplay()
    {
        enemy.GetComponent<AnimatorManager>().PlayAnimByKey("moveForward");
        enemy.transform.Translate(Vector3.forward * Time.deltaTime * 0.5f );
    }

    private void AttackEnemy()
    {
        enemy.GetComponent<AnimatorManager>().PlayAnimByKey("attack1");
        damegeAnim.Play("Damege");
    }


    private void CapsuleDisplay()
    {
        TestServer.instance.CallFunc();
        if (isAlice)
        {
            GameManager.instance.EndOfGame("Alice");
        }
    }
}
