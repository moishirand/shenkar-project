﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public interface ICharacter
{
    int Health { get; set; }
    Transform transform { get; set; }

    void Attack();
    void Damege();


}
