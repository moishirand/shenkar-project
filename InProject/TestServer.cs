﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class TestServer : MonoBehaviour {
    public static TestServer instance;
    public bool isAlic;
    public Text text;
    public GameObject arCamera;

    private PhotonView photonView;

    private void Awake()
    {
        if(instance != null && instance != this)
        {
            DestroyImmediate(gameObject);
        }
        instance = this;
    }

    private void Start()
    {
        photonView = GetComponent<PhotonView>();
    }

    public void CallFunc()
    {
        SendToServer();
    }

    private void SendToServer()
    {
        text.gameObject.SetActive(true);
        text.text = "Alice";
        //if (isAlic)
        //{
        //    str = "Alice";
        //}
        //else
        //{
        //    str = "Bob";
        //}

        string str = "Bob";

        photonView.RPC("FinishText", PhotonTargets.Others, str);
    }

    [PunRPC]
    private void FinishText(string finish)
    {
        text.text = finish;       
    }
}
