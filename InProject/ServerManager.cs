﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using ExitGames.Client.Photon;
using System;

public class ServerManager : Photon.MonoBehaviour
{
    private const string roomName = "RoomName";
    private RoomInfo[] roomsList;

    // Use this for initialization
    void Start () {
        PhotonNetwork.ConnectUsingSettings("0.1");

        //now connect when the gmae start
        //Connect();
    }

    public void Connect()
    {
        RoomOptions roomOptions = new RoomOptions();
        roomOptions.IsVisible = false;
        roomOptions.MaxPlayers = 4;
        PhotonNetwork.JoinOrCreateRoom(roomName, roomOptions, TypedLobby.Default);
    }

    void OnReceivedRoomListUpdate()
    {
        roomsList = PhotonNetwork.GetRoomList();
    }

    void OnJoinedRoom()
    {
        //changeNumber.clients++;
        Debug.Log("Connected to Room");
    }

    // Update is called once per frame
    void Update () {
		
	}
}
