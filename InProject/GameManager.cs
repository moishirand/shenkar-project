﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameManager : MonoBehaviour {
    public static GameManager instance;

    [SerializeField]
    private GameObject bob;
    [SerializeField]
    private GameObject alice;

    private void Awake()
    {
        if (instance != null && instance != this)
        {
            DestroyImmediate(gameObject);
        }

        instance = this;
    }

    public GameObject GetBob()
    {
        return bob;
    }

    public GameObject GetAlice()
    {
        return alice;
    }

    public void EndOfGame(string tag)
    {
        //if (tag == "Alice")
        //{
        //    alice.GetComponent<DesplayWinOrLose>().DisplayResult("Alice, You Win!");
        //    bob.GetComponent<DesplayWinOrLose>().DisplayResult("Bob, You Lose!");
        //}

        //if(tag == "Bob")
        //{
        //    alice.GetComponent<DesplayWinOrLose>().DisplayResult("Alice, You Lose!");
        //    bob.GetComponent<DesplayWinOrLose>().DisplayResult("Bob, You Win!");
        //}

        StartCoroutine(DelayBeforeStopGame());
    }


    private IEnumerator DelayBeforeStopGame()
    {
        yield return new WaitForSeconds(2);
        Time.timeScale = 0;
    }



}
