﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Events;
using GoogleARCore.Examples.CloudAnchors;
using System;

public class UIManager : MonoBehaviour {

    public static UIManager instance;
    //[SerializeField]
    //private Text clientsNumber;
    //[SerializeField]
    //private CloudAnchorController cloudAnchor;
    private float numberOfClient;

    [SerializeField]
    private Text entity;

    private void Awake()
    {
        if(instance != null && instance != this)
        {
            DestroyImmediate(gameObject);
        }

        instance = this;
    }



    // Use this for initialization
    void Start () {
        //cloudAnchor.action += OnAddClient;

    }

    

    public void AddEntityText(string name)
    {
        entity.text = name;
    }



}
