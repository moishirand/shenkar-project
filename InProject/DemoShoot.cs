﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DemoShoot : MonoBehaviour {
    public GameObject arrow;

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
        if (Input.GetKeyDown(KeyCode.Space))
        {
            arrow.SetActive(true);
            arrow.GetComponentInChildren<Rigidbody>().AddForce(Vector3.forward * Time.deltaTime * 1000);
        }
	}
}
