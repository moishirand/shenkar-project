﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public  class AnimatorManager : MonoBehaviour {

    private Animator anim;

	// Use this for initialization
	void Start () {
        anim = GetComponent<Animator>();
        anim.Play("idle");
	}

    public void PlayAnimByKey(string key)
    {
        anim.Play(key);
    }
	
	
}
